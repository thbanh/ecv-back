const express = require("express");
var cors = require("cors");
const app = express();
const port = 4000;

app.use(cors());

app.get("/", (req, res) => {
  res.send("Merci d'appeler /allo");
});

app.get("/allo", (req, res) => {
  const currentDate = new Date().toISOString();
  const myMessage = "Moshi Moshi";
  let jsonResp = { msg: myMessage, date: currentDate };
  console.log("Return json", jsonResp);
  res.json(jsonResp);
});

const addr = "localhost";
// const addr = "127.0.0.1";
// const addr = "0.0.0.0";
app.listen(port, addr, () => {
  console.log(`Allo SSR app écoute sur l'adresse ${addr} et le port ${port}`);
});
